#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2022 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the Eclipse Public License 2.0
# which is available at http://www.eclipse.org/legal/epl-v20.html
# SPDX-License-Identifier: EPL-2.0
#*******************************************************************************

set -euo pipefail

# Outputs the list of redirect from the given url, e.g.
# $ ./list-redirect.sh polarsys.org:80
# https://www.eclipse.org/polarsys|hsts|63072000
# https://www.eclipse.org/polarsys/|hsts|63072000
# http://projects.eclipse.org/projects/polarsys|no-hsts|0
# https://projects.eclipse.org/projects/polarsys|hsts|63072000
url="${1}"
headers="$(mktemp)"

while [[ -n "${url}" ]]; do
  if ! status=$(curl -sk --connect-timeout 10 -o /dev/null -D "${headers}" -w '%{http_code}' "${url}"); then
    break
  fi

  if [[ "${1}" != "${url}" ]]; then
    hsts="$(grep -i strict-transport-security <"${headers}" | tr -d '\r' || :)"
    maxAge="$(sed -E 's/.*max-age=([0-9]+).*/\1/g' <<<"${hsts}")"
    if [[ -n "${hsts:-}" ]]; then
      echo "${url}|hsts|${maxAge}"
    else
      echo "${url}|no-hsts|0"
    fi
  fi

  if [[ ${status} -ge 300 ]] && [[ ${status} -lt 400 ]]; then
    url="$(grep -i location <"${headers}" | sed -E 's/^location *: *(.*)$/\1/gi' | tr -d '\r' || :)"
  else
    url=""
  fi

done

rm -rf "${headers}"
# sitescan - Scan a site for sane and safe TLS/SSL, redirects, and headers

## How to run?

Simply call the script `sitescan.sh` with the site you want to check. Domain can be `host:port` or just `host` (port default is `:443`).

```bash
./sitescan.sh www.eclipse.org
OK (www.eclipse.org): valid cert
OK (www.eclipse.org): ssl v2 disabled
OK (www.eclipse.org): ssl v3 disabled
OK (www.eclipse.org): tls v1.0 disabled
OK (www.eclipse.org): tls v1.1 disabled
OK (www.eclipse.org): optional tls v1.2 enabled
OK (www.eclipse.org): optional tls v1.3 disabled
OK (www.eclipse.org): compression disabled
OK (www.eclipse.org): TLS Fallback SCSV supported
OK (www.eclipse.org): renegotiation not supported
OK (www.eclipse.org): hsts enabled >= 2y
OK (www.eclipse.org): safe redirect(s) (all https+hsts)
KO (www.eclipse.org:80): no redirect to https
```

## Requirements

* Recent value of OpenSSL (tested with 1.1.1q)
* [sslscan](https://github.com/rbsec/sslscan) (
  * tested with 2.0.15 but does not support `compression` check (returns false positives)
  * tested with [static build](https://github.com/rbsec/sslscan#building-on-linux) of [c450fb1](https://github.com/rbsec/sslscan/commit/c450fb1331dd3ba48b31eedf3c33082ea52d3848)

## Trademarks

Eclipse® is a Trademark of the Eclipse Foundation, Inc.
Eclipse Foundation is a Trademark of the Eclipse Foundation, Inc.

## Copyright and license

Copyright 2020 the [Eclipse Foundation, Inc.](https://www.eclipse.org) and the [cerberus authors](https://github.com/eclipsefdn/cerberus/graphs/contributors). Code released under the [Eclipse Public License Version 2.0 (EPL-2.0)](https://github.com/eclipsefdn/cerberus/blob/src/LICENSE).
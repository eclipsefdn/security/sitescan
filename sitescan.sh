#!/usr/bin/env bash
#*******************************************************************************
# Copyright (c) 2022 Eclipse Foundation and others.
# This program and the accompanying materials are made available
# under the terms of the Eclipse Public License 2.0
# which is available at http://www.eclipse.org/legal/epl-v20.html
# SPDX-License-Identifier: EPL-2.0
#*******************************************************************************

set -euo pipefail

# Ouputs a list of diagnosis for a website supporting good default
# TLS configuration (:port is optional, default is :443), e.g.:
# $ ./sitescan.sh www.eclipse.org
# OK (www.eclipse.org): valid cert
# OK (www.eclipse.org): ssl v2 disabled
# OK (www.eclipse.org): ssl v3 disabled
# OK (www.eclipse.org): tls v1.0 disabled
# OK (www.eclipse.org): tls v1.1 disabled
# OK (www.eclipse.org): optional tls v1.2 enabled
# OK (www.eclipse.org): optional tls v1.3 disabled
# OK (www.eclipse.org): compression disabled
# OK (www.eclipse.org): TLS Fallback SCSV supported
# OK (www.eclipse.org): renegotiation not supported
# OK (www.eclipse.org): hsts enabled >= 2y
# OK (www.eclipse.org): safe redirect(s) (all https+hsts)
# KO (www.eclipse.org:80): no redirect to https

RED=""
GREEN=""
YELLOW=""
NC=""
if test -t 1; then
  RED='\033[0;31m'
  GREEN='\033[0;32m'
  YELLOW='\033[0;33m'
  NC='\033[0m' # No Color
fi

is_supported_protocol() {
  local sslscan_xml="${1}"
  local type="${2}"
  local version="${3}"
  [[ "$(xml sel -t -c 'string(/document/ssltest/protocol[@type="'"${type}"'"][@version="'"${version}"'"]/@enabled)' <"${sslscan_xml}")" == "1" ]]
}

verify_cert() {
  local h="${1}"
  local p="${2}"
  local isHostname="${3}"

  if [[ "${isHostname}" == "true" ]]; then
    if ! openssl s_client -verify_return_error -connect "${h}:${p:-443}" -servername "${h}" </dev/null >/dev/null 2>&1 || \
      ! openssl s_client -verify_return_error -verify_hostname "${h}" -connect "${h}:${p:-443}" -servername "${h}" </dev/null >/dev/null 2>&1; then
      echo -e "${RED}KO (${h}:${p}): invalid cert${NC}"
    else
      echo -e "${GREEN}OK (${h}:${p}): valid cert${NC}"
    fi
  else
    if ! openssl s_client -verify_return_error -connect "${h}:${p:-443}" </dev/null >/dev/null 2>&1; then
      echo -e "${RED}KO (${h}:${p}): expired cert${NC}"
    else
      echo -e "${GREEN}OK (${h}:${p}): non expired cert${NC}"
    fi
  fi
}

report_supported_protocol() {
  local sslscan_xml="${1}"
  local type="${2}"
  local version="${3}"
  local deprecated="${4}"
  local optional="${5:-"false"}"
  if ! is_supported_protocol "${sslscan_xml}" "${type}" "${version}"; then
    if [[ "${deprecated}" == "true" ]]; then
      echo -e "${GREEN}OK (${hostport}): ${type} v${version} disabled${NC}"
    elif [[ "${optional}" == "false" ]]; then
      echo -e "${RED}KO (${hostport}): ${type} v${version} disabled${NC}"
    else
      echo -e "${YELLOW}OK (${hostport}): optional ${type} v${version} disabled${NC}"
    fi
  else
    if [[ "${deprecated}" == "true" ]]; then
      echo -e "${RED}KO (${hostport}): ${type} v${version} enabled${NC}"
    elif [[ "${optional}" == "false" ]]; then
      echo -e "${GREEN}OK (${hostport}): ${type} v${version} enabled${NC}"
    else
      echo -e "${GREEN}OK (${hostport}): optional ${type} v${version} enabled${NC}"
    fi
  fi
}

report_renegotiation() {
  local sslscan_xml="${1}"
  local hostport="${2}"

  if [[ "$(xml sel -t -c 'string(/document/ssltest/renegotiation/@supported)' <"${sslscan_xml}")" == "0" ]]; then
    echo -e "${GREEN}OK (${hostport}): renegotiation not supported${NC}"
  elif [[ "$(xml sel -t -c 'string(/document/ssltest/renegotiation/@secure)' <"${sslscan_xml}")" == "1" ]]; then
    echo -e "${GREEN}OK (${hostport}): secure session renegotiation supported${NC}"
  else
    echo -e "${RED}KO (${hostport}): secure session renegotiation NOT supported${NC}"
  fi
}

report_fallback() {
  local sslscan_xml="${1}"
  local hostport="${2}"

  if [[ "$(xml sel -t -c 'string(/document/ssltest/fallback/@supported)' <"${sslscan_xml}")" == "1" ]]; then
    echo -e "${GREEN}OK (${hostport}): TLS Fallback SCSV supported${NC}"
  else
    echo -e "${RED}KO (${hostport}): TLS Fallback SCSV NOT supported${NC}"
  fi
}

report_compression() {
  local sslscan_xml="${1}"
  local hostport="${2}"

  if [[ "$(xml sel -t -c 'string(/document/ssltest/compression/@supported)' <"${sslscan_xml}")" == "0" ]]; then
    echo -e "${GREEN}OK (${hostport}): compression disabled${NC}"
  else
    echo -e "${RED}KO (${hostport}): compression enabled${NC}"
  fi
}

report_redirect() {
  scheme="${1}"
  hostport="${2}"
  redirects="$(./list-redirect.sh "${scheme}://${hostport}")"
  if [[ -z "${redirects:-}" && "${scheme}" == "http" ]]; then
    echo -e "${RED}KO (${hostport}): no redirect to https${NC}"
  elif grep -q "http://" <<<"${redirects}"; then
    echo -e "${RED}KO (${hostport}): unsafe redirect(s) to plain http${NC}"
  else
    if grep -q "no-hsts" <<<"${redirects}"; then
      echo -e "${RED}KO (${hostport}): unsafe redirect(s) (no hsts on at least 1)${NC}"
    else
      echo -e "${GREEN}OK (${hostport}): safe redirect(s) (all https+hsts)${NC}"
    fi
  fi
}

report_hsts() {
  hostport="${1}"
  hsts="$(curl -sSk -o /dev/null -D - --connect-timeout 10 "https://${hostport}" | grep -i strict-transport-security | tr -d '\r' || :)"
  if [[ -n "${hsts:-}" ]]; then
    maxAge=0
    maxAge="$(sed -E 's/.*max-age=([0-9]+).*/\1/g' <<<"${hsts}")"
    if [[ ${maxAge} -lt 63072000 ]]; then
      echo -e "${YELLOW}OK (${hostport}): hsts enabled < 2y${NC}"
    else
      echo -e "${GREEN}OK (${hostport}): hsts enabled >= 2y${NC}"
    fi
  else
    echo -e "${RED}KO (${hostport}): hsts disabled${NC}"
  fi
}

hostport="${1}"

# https://stackoverflow.com/a/17871737
IP_V4='([0-9]{1,3}\.){3}[0-9]{1,3}(:[0-9]{1,4})?'
IP_V6='([0-9a-fA-F]{1,4}:){1,7}[0-9a-fA-F]{1,4}'
IP_V6_PORT='\['"${IP_V6}"'\]:[0-9]{1,4}'

if [[ "${hostport}" =~ ^${IP_V4}$ ]]; then
  h="$(sed -E 's/([^:]+)(:([0-9]+))?/\1/g' <<<"${hostport}")"
  p="$(sed -E 's/([^:]+)(:([0-9]+))?/\3/g' <<<"${hostport}")"
elif [[ ${hostport} =~ ^${IP_V6_PORT}$ ]]; then
  h="$(sed -E 's/\[([0-9a-zA-Z:]+)\]:([0-9]+)/\1/g' <<<"${hostport}")"
  p="$(sed -E 's/\[([0-9a-zA-Z:]+)\]:([0-9]+)/\2/g' <<<"${hostport}")"
elif [[ "${hostport}" =~ ^${IP_V6}$ ]]; then
  h="${hostport}"
else
  isHostname="true"
  h="$(sed -E 's/([^:]+)(:([0-9]+))?/\1/g' <<<"${hostport}")"
  p="$(sed -E 's/([^:]+)(:([0-9]+))?/\3/g' <<<"${hostport}")"

  if [[ -z "$(dig +short "${h}")" ]]; then
    echo -e "${YELLOW}KO (${hostport}): could not resolve hostname${NC}"
    exit 0
  fi
fi

sslscan_xml="$(mktemp)"

sslscan --connect-timeout=10 --xml=- --no-check-certificate --no-ciphersuites --no-groups --no-heartbleed "${hostport}" > "${sslscan_xml}"

if is_supported_protocol "${sslscan_xml}" "ssl" "2" || \
  is_supported_protocol "${sslscan_xml}" "ssl" "3" || \
  is_supported_protocol "${sslscan_xml}" "tls" "1.0" || \
  is_supported_protocol "${sslscan_xml}" "tls" "1.1" || \
  is_supported_protocol "${sslscan_xml}" "tls" "1.2" || \
  is_supported_protocol "${sslscan_xml}" "tls" "1.3"; then

  verify_cert "${h}" "${p}" "${isHostname:-}"

  report_supported_protocol "${sslscan_xml}" "ssl" "2" "true"
  report_supported_protocol "${sslscan_xml}" "ssl" "3" "true"
  report_supported_protocol "${sslscan_xml}" "tls" "1.0" "true"
  report_supported_protocol "${sslscan_xml}" "tls" "1.1" "true"
  report_supported_protocol "${sslscan_xml}" "tls" "1.2" "false" "true"
  report_supported_protocol "${sslscan_xml}" "tls" "1.3" "false" "true"

  if ! is_supported_protocol "${sslscan_xml}" "tls" "1.2" && \
    ! is_supported_protocol "${sslscan_xml}" "tls" "1.3"; then
    echo -e "${RED}KO (${hostport}): neither tls v1.2 nor v1.3 is enabled${NC}"
  fi

  report_compression "${sslscan_xml}" "${hostport}"
  report_renegotiation "${sslscan_xml}" "${hostport}"
  report_fallback "${sslscan_xml}" "${hostport}"

  if [[ "${isHostname:-}" == "true" ]]; then
    report_hsts "${hostport}"
    report_redirect "https" "${hostport}"
    report_redirect "http" "${h}:80"
  fi
elif [[ "${isHostname:-}" == "true" ]]; then
  report_redirect "http" "${hostport}"
else
  echo -e "${YELLOW}OK (${h}${p}): non TLS IP${NC}"
fi

rm -f "${sslscan_xml}"
